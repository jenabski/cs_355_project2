var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM gamer;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(gamer_id, callback) {
    var query = 'SELECT c.*, a.UMG_username, a.cmg_username FROM gamer c ' +
        'LEFT JOIN gamer_username ca on ca.gamer_id = c.gamer_id ' +
        'LEFT JOIN username a on a.username_id = ca.username_id ' +
        'WHERE c.gamer_id = ?';
    var queryData = [gamer_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO gamer (gamer_name) VALUES (?)';

    var queryData = [params.gamer_name];

    connection.query(query, params.gamer_name, function(err, result) {


        var gamer_id = result.insertId;


        var query = 'INSERT INTO gamer_username (gamer_id, username_id) VALUES ?';


        var gamerusernameData = [];
        if (params.username_id.constructor === Array) {
            for (var i = 0; i < params.username_id.length; i++) {
                gamerusernameData.push([gamer_id, params.username_id[i]]);
            }
        }
        else {
            gamerusernameData.push([gamer_id, params.username_id]);
        }


        connection.query(query, [gamerusernameData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(gamer_id, callback) {
    var query = 'DELETE FROM gamer WHERE gamer_id = ?';
    var queryData = [gamer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


var gamerusernameInsert = function(gamer_id, usernameIdArray, callback){

    var query = 'INSERT INTO gamer_username (gamer_id, username_id) VALUES ?';


    var gamerusernameData = [];
    if (usernameIdArray.constructor === Array) {
        for (var i = 0; i < usernameIdArray.length; i++) {
            gamerusernameData.push([gamer_id, usernameIdArray[i]]);
        }
    }
    else {
        gamerusernameData.push([gamer_id, usernameIdArray]);
    }
    connection.query(query, [gamerusernameData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.gamerusernameInsert = gamerusernameInsert;

//declare the function so it can be used locally
var gamerusernameDeleteAll = function(gamer_id, callback){
    var query = 'DELETE FROM gamer_username WHERE gamer_id = ?';
    var queryData = [gamer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.gamerusernameDeleteAll = gamerusernameDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE gamer SET gamer_name = ? WHERE gamer_id = ?';
    var queryData = [params.gamer_name, params.gamer_id];

    connection.query(query, queryData, function(err, result) {

        gamerusernameDeleteAll(params.gamer_id, function(err, result){

            if(params.username_id != null) {

                gamerusernameInsert(params.gamer_id, params.username_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};


exports.edit = function(gamer_id, callback) {
    var query = 'CALL gamer_getinfo(?)';
    var queryData = [gamer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};