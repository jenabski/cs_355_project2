var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM gamertags;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(gamertag_id, callback){
    var query = 'delete from gamertags where gamertag_id = ' + gamertag_id;

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(gamertagsObj, callback)
{
    var id = gamertagsObj.gamertag_id;
    var gamertags = gamertagsObj.gamertags_name;
    var PSN_gamertags = gamertagsObj.PSN_gamertags;
    var query = "update gamertags set gamertags_name = " + gamertags + ", PSN_gamertags = " + PSN_gamertags + "where gamertag_id = " + id;
    connection.query(query, function (err, result)
    {
        callback(err, result);
    });
};