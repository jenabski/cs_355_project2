var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback)
{
    var query = 'SELECT * FROM username;';

    connection.query(query, function(err, result)
    {
        console.log("username_dal getAll result: " + result);

        callback(err, result);
    });
};

exports.delete = function(username_id, callback)
{
    var query = 'delete from username where username_id = ' + username_id;

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};

exports.edit = function(usernameObj, callback)
{
    var id = usernameObj.username_id;
    var UMG_username = usernameObj.UMG_username;
    var CMG_username = usernameObj.CMG_username;
    var query= "update username set UMG_username = '" + UMG_username + "', CMG_username code = " + CMG_username + " where username_id = " + id + ";";
    console.log(query);
    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};