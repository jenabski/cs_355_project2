var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);

exports.getAll = function(callback)
{
    var query = 'SELECT * FROM dispute;';

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};

exports.delete = function(dispute_id, callback)
{
    var query = 'delete from dispute where dispute_id = ' + dispute_id;

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};

exports.edit = function(usernameObj, callback){
    var id = usernameObj.dispute_id;
    var dispute = usernameObj.dispute_percentage;
    var username_id = usernameObj.username_id;
    var query= "update dispute set dispute_percentage = '" + dispute + "', username_id = " + username_id + " where dispute_id = " + id +";";
    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};