var express = require('express');
var router = express.Router();
var insert = require('../model/carbine');
var username_dal = require('../model/username_dal');

router.get('/all', function(req, res)
{
    username_dal.getAll(function (err, response)
    {
        console.log(response);
        res.render("username/usernameViewAll", {response: response});
    });
});

router.post('/all', function(req, res)
{
    console.log("post req called");
    console.log(req.body.username);
    insert.carbine("username", req.body.username, function(err, result)
    {
        if(err)
        {
            res.send(err);
        }
        else
            {
            res.redirect('/username/all');
        }
    });
});

router.get('/delete/:id', function(req, res)
{
    console.log(req.params.id);
    username_dal.delete(req.params.id, function(err, result)
    {
        if(err)
        {
            res.send(err);
        }
        else
            {
            res.redirect('/username/all');
        }
    });
});

router.post('/edit', function(req,res)
{
    console.log("post req called");
    console.log(req.body.username);
    username_dal.edit(req.body.username, function(err, result)
    {
        if(err)
        {
            console.log("edit threw error");
            res.send(err);

        }
        else
            {
            res.redirect('/username/all');
        }
    });
});
module.exports = router;