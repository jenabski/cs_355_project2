var express = require('express');
var router = express.Router();
var insert = require('../model/carbine');
var gamertags_dal = require('../model/gamertags_dal');

router.get('/all', function(req, res)
{
    gamertags_dal.getAll(function (err, response)
    {
        console.log(response);
        res.render("gamertags/gamertagsViewAll", {response: response});
    });
});

router.post('/all', function(req, res)
{
    console.log("post req called");
    console.log(req.body.gamertags);
    insert.carbine("gamertags", req.body.gamertags, function(err, result)
    {
        if(err)
        {
            res.send(err);
        }else
            {
            res.redirect('/gamertags/all');
        }
    });
});

router.get('/delete/:id', function(req, res)
{
    var id = req.params.id;
    console.log(req.params.id);
    gamertags_dal.delete(id, function(err, result)
    {
        if(err)
        {
            res.send(err);
        }
        else
            {
            res.redirect('/gamertags/all');
        }
    });
});


router.post('/edit', function(req,res)
{
    console.log("post req called");
    console.log(req.body.gamertags);
    gamertags_dal.edit(req.body.gamertags, function(err, result)
    {
        if(err)
        {
            console.log("edit threw error");

        }
        else
            {
            res.redirect('/gamertags/all');
        }
    });
});
module.exports = router;