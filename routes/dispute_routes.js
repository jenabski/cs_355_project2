var express = require('express');
var router = express.Router();
var insert = require('../model/carbine');
var dispute_dal = require('../model/dispute_dal');

router.get('/all', function(req, res)
{
    dispute_dal.getAll(function (err, response)
    {
        console.log(response);
        res.render("dispute/disputeViewAll", {response: response});
    });
});

router.post('/all', function(req, res)
{
    console.log("post req called");
    console.log(req.body.dispute);
    insert.carbine("dispute", req.body.dispute, function(err, result)
    {
        if(err)
        {
            res.send(err);
        }
        else
            {
            res.redirect('/dispute/all');
        }
    });
});

router.get('/delete/:name', function(req, res)
{
    console.log(req.params.name);
    dispute_dal.delete(req.params.name, function(err, result)
    {
        if(err)
        {
            res.send(err);
        }
        else
            {
            res.redirect('/dispute/all');
        }
    });
});

router.post('/edit', function(req,res)
{
    console.log("post req called");
    console.log(req.body.dispute);
    dispute_dal.edit(req.body.dispute, function(err, result)
    {
        if(err)
        {
            console.log("Query" + query);
            console.log("edit threw error");

        }
        else
            {
            res.redirect('/dispute/all');
        }
    });
});
module.exports = router;
