var express = require('express');
var router = express.Router();
var gamer_dal = require('../model/gamer_dal');
var gamer_dal = require('../model/username_dal');



router.get('/all', function(req, res) {
    gamer_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('gamer/gamerViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.gamer_id == null) {
        res.send('gamer_id is null');
    }
    else {
        gamer_dal.getById(req.query.gamer_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('gamer/gamerViewById', {'result': result});
           }
        });
    }
});


router.get('/add', function(req, res){

    gamer_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('gamer/gamerAdd', {'gamer': result});
        }
    });
});


router.get('/insert', function(req, res)
{
    if(req.query.gamer_name == null) {
        res.send('gamer Name must be provided.');
    }
    else if(req.query.gamer_id == null) {
        res.send('At least one gamer must be selected');
    }
    else
        {

        gamer_dal.insert(req.query, function(err,result)
        {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else
                {

                res.redirect(302, '/gamer/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.gamer_id == null) {
        res.send('A gamer id is required');
    }
    else
        {
        gamer_dal.edit(req.query.gamer_id, function(err, result){
            res.render('gamer/gamerUpdate', {gamer: result[0][0], gamer: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.gamer_id == null) {
       res.send('A gamer id is required');
   }
   else {
       gamer_dal.getById(req.query.gamer_id, function(err, gamer){
           gamer_dal.getAll(function(err, gamer) {
               res.render('gamer/gamerUpdate', {gamer: gamer[0], gamer: gamer});
           });
       });
   }

});

router.get('/update', function(req, res) {
    gamer_dal.update(req.query, function(err, result){
       res.redirect(302, '/gamer/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.gamer_id == null) {
        res.send('gamer_id is null');
    }
    else {
         gamer_dal.delete(req.query.gamer_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {

                 res.redirect(302, '/gamer/all');
             }
         });
    }
});

module.exports = router;
